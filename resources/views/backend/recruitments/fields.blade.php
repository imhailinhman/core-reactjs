<!-- Position Field -->
<div class="form-group col-sm-6">
    {!! Form::label('position', 'Position:') !!}
    {!! Form::text('position', null, ['class' => 'form-control']) !!}
</div>

<!-- Alias Field -->
<div class="form-group col-sm-6">
    {!! Form::label('alias', 'Alias:') !!}
    {!! Form::text('alias', null, ['class' => 'form-control']) !!}
</div>

<!-- Quantity Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('quantity', 'Quantity:') !!}
    {!! Form::textarea('quantity', null, ['class' => 'form-control']) !!}
</div>

<!-- Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('type', 'Type:') !!}
    {!! Form::text('type', null, ['class' => 'form-control']) !!}
</div>

<!-- Experience Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('experience', 'Experience:') !!}
    {!! Form::textarea('experience', null, ['class' => 'form-control']) !!}
</div>

<!-- Salary Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('salary', 'Salary:') !!}
    {!! Form::textarea('salary', null, ['class' => 'form-control']) !!}
</div>

<!-- Diploma Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('diploma', 'Diploma:') !!}
    {!! Form::textarea('diploma', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
</div>

<!-- Benefit Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('benefit', 'Benefit:') !!}
    {!! Form::textarea('benefit', null, ['class' => 'form-control']) !!}
</div>

<!-- Requirement Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('requirement', 'Requirement:') !!}
    {!! Form::textarea('requirement', null, ['class' => 'form-control']) !!}
</div>

<!-- Profile Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('profile', 'Profile:') !!}
    {!! Form::textarea('profile', null, ['class' => 'form-control']) !!}
</div>

<!-- Image Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('image', 'Image:') !!}
    {!! Form::textarea('image', null, ['class' => 'form-control']) !!}
</div>

<!-- Thumb Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('thumb', 'Thumb:') !!}
    {!! Form::textarea('thumb', null, ['class' => 'form-control']) !!}
</div>

<!-- Time Out Field -->
<div class="form-group col-sm-6">
    {!! Form::label('time_out', 'Time Out:') !!}
    {!! Form::date('time_out', null, ['class' => 'form-control']) !!}
</div>

<!-- Time Field -->
<div class="form-group col-sm-6">
    {!! Form::label('time', 'Time:') !!}
    {!! Form::date('time', null, ['class' => 'form-control']) !!}
</div>

<!-- Category Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('category_id', 'Category Id:') !!}
    {!! Form::number('category_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('status', false) !!}
        {!! Form::checkbox('status', '1', null) !!} 1
    </label>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('recruitments.index') !!}" class="btn btn-default">Cancel</a>
</div>
