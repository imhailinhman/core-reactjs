<div class="col-sm-12">
    <div class="box box-success mb-15">
        <div class="box-header">
            <h3 class="box-title">Ảnh chi tiết tour</h3>
            <!-- tools box -->
            <div class="pull-right box-tools">
                <button type="button" class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
            </div>
            <!-- /. tools -->
        </div>
        <div class="box-body pad">
            <input id="file-1" name="multiple_file[]" type="file"  multiple data-msg-placeholder="Select {files} for upload..." value="">
        </div>
    </div>
    <div class="box box-info">
        <div class="box-header">
            <h3 class="box-title">Thông tin tour</h3>
            <!-- tools box -->
            <div class="pull-right box-tools">
                <button type="button" class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
            </div>
            <!-- /. tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body pad">
            <div class="form-group col-md-12 col-sm-12 col-xs-12 text-center">
                <img id="img-upload" class="img-fluid center-block" width="200px" height="200px"/>
                <div id="img-upload-remove">
                    @if (isset($model->image))
                        <img src="{{$model->image }}" class=" img-fluid img-thumbnail center-block "
                             width="200px" height="200px">
                        <input type="hidden" name="image_tmp" value="{{$model->image}}"/>
                    @else
                        <img src="{{ config('filepath.no_image') }}" class="  img-fluid img-thumbnail center-block "
                             width="200px" height="200px">

                    @endif
                </div>

                <div class="form-group col-md-offset-5 col-sm-offset-5 col-md-2 col-sm-2 col-xs-12 text-center">
                    {!! Html::decode(Form::label('image','Ảnh',['class' => 'font-weight-bold float-left mr-4'])) !!}
                    {!! Form::file('image', ['class' => 'form-control w-75', 'accept' => 'image/*', 'name' => 'image', 'placeholder' => 'Ảnh', 'id' => 'image' ]) !!}
                </div>
            </div>

            <!-- Title Field -->
            <div class="form-group col-sm-6">
                {!! Form::label('name', 'Tên tour:') !!}
                <span class="text-danger">(*)</span>
                {!! Form::text('name', (!empty($model)) ? $model->name : old('name'), ['placeholder'=>'Tên sản phẩm', 'class' => 'form-control']) !!}
            </div>

            <!-- company_id Field -->
            <div class="form-group col-sm-6">
                {!! Form::label('category_id', 'Menu:') !!}
                <span class="text-danger">(*)</span>
                <select name="category_id" id="category_id" class="form-control select2">
                    <option value="">-------- Menu --------</option>
                    @if(!empty($categores))
                        @foreach($categores as $k_id => $category)
                            <?php
                            if (!empty($model)) {
                                $selected = ($k_id == $model->category_id) ? 'selected' : null;
                            } else {
                                $selected = '';
                            }
                            ?>
                            <option {{$selected}} value="{{ $k_id }}" {{ (old("category_id") == $k_id ? "selected":"") }}>{{ $category }}</option>
                        @endforeach
                    @endif
                </select>
            </div>

            <div class="form-group col-sm-6">
                {!! Form::label('name', 'Giá tour:') !!}
                <input value="{{ !empty($model->price) ? $model->price : old('price') }}" class="form-control text-right price-input" type="text" name="price" placeholder="0" minlength="4"
                       data-inputmask="'alias': 'currency', 'suffix': ' vnđ', 'prefix': '', 'digits': 0, 'groupSeparator': ',', 'removeMaskOnSubmit': true, 'autoUnmask': true">
            </div>

            <div class="form-group col-sm-6">
                {!! Form::label('price_sale', 'Giá tour giảm:') !!}
                <input value="{{ !empty($model->price_sale) ? $model->price_sale : old('price_sale') }}" class="form-control text-right price-input" type="text" name="price_sale" placeholder="0" minlength="4"
                       data-inputmask="'alias': 'currency', 'suffix': ' vnđ', 'prefix': '', 'digits': 0, 'groupSeparator': ',', 'removeMaskOnSubmit': true, 'autoUnmask': true">
            </div>

            <!-- Thời gian khởi hành  -->
            <div class="form-group col-sm-6 col-xs-6">
                {!! Form::label('date_start', 'Thời gian khởi hành : ', array()) !!}
                <span class="text-danger">(*)</span>
                <div class='input-group date' id='datetimepicker'  data-date-format='dd-mm-yyyy'>
                    <input name="date_start" type='text' class="form-control"
                           value="{!! (!empty($model) && $model->date_start) ? Helper::formatDate($model->date_start, 'd-m-Y') : old('date_start') !!}" required/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>

            <!-- Thời gian khởi hành  -->
            <div class="form-group col-sm-6 col-xs-6">
                {!! Form::label('date_end', 'Thời gian kết thúc : ', array()) !!}
                <span class="text-danger">(*)</span>
                <div class='input-group date' id='date_end' data-date-format='dd-mm-yyyy'>
                    <input name="date_end" type='text' class="form-control"
                           value="{!! (!empty($model) && $model->date_end) ? Helper::formatDate($model->date_end, 'd-m-Y') : old('date_end') !!}" required/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>

            <div class="form-group col-sm-6">
                {!! Form::label('start_point', 'Điểm khởi hành:') !!}
                <span class="text-danger">(*)</span>
                {!! Form::select('start_point', $province , (!empty($model)) ? $model->start_point : old('start_point'), ['class' => 'form-control select2', 'required']) !!}
            </div>

            <div class="form-group col-sm-6">
                {!! Form::label('end_point', 'Điểm đến:') !!}
                <span class="text-danger">(*)</span>
                {!! Form::select('end_point', $province , (!empty($model)) ? $model->end_point : old('end_point'), ['class' => 'form-control select2']) !!}
            </div>

            <div class="form-group col-sm-6">
                {!! Form::label('transport', 'Phương tiện vận chuyển:') !!}
                {!! Form::select('transport', $transport , (!empty($model)) ? $model->transport : old('transport'), ['class' => 'form-control select2']) !!}
            </div>

            <div class="form-group col-sm-6">
                {!! Form::label('type', 'Loại tour:') !!}
                {!! Form::select('type', $typeTour , (!empty($model)) ? $model->type : old('type'), ['class' => 'form-control select2']) !!}
            </div>

            <!-- Title Field -->
            <div class="form-group col-sm-6">
                {!! Form::label('seat', 'Số ghế:') !!}
                <span class="text-danger">(*)</span>
                {!! Form::number('seat', (!empty($model)) ? $model->seat : old('seat'), ['placeholder'=>'Số ghế', 'class' => 'form-control']) !!}
            </div>

            <!-- Keyword seo Field -->
            <div class="form-group col-sm-6">
                {!! Form::label('keyword_seo', 'Keyword seo:') !!}
                <span class="text-danger">(*)</span>
                {!! Form::text('keyword_seo', (!empty($model)) ? $model->keyword_seo : old('keyword_seo'), ['placeholder'=>'Keyword seo', 'class' => 'form-control']) !!}
            </div>

            {{--<!-- Title Field -->--}}
            {{--<div class="form-group col-sm-6">--}}
                {{--{!! Form::label('follow', 'Lượng theo dõi:') !!}--}}
                {{--<span class="text-danger">(*)</span>--}}
                {{--{!! Form::number('follow', (!empty($model)) ? $model->follow : old('follow'), ['placeholder'=>'Lượng theo dõi', 'class' => 'form-control']) !!}--}}
            {{--</div>--}}

            <!-- Description Field -->
            <div class="form-group col-sm-12 col-lg-12">
                {!! Form::label('description', 'Tóm tắt:') !!}
                {!! Form::textarea('description', (!empty($model)) ? $model->description : old('description'), ['placeholder'=>'Tóm tắt', 'class' => 'form-control', 'rows' => 2]) !!}
            </div>

            <!-- Status Field -->
            <div class="form-group col-sm-12 col-xs-12 col-md-12">
                {!! Form::label('status', 'Trạng thái :') !!}
                <div class="material-switch float-left">
                    {!! Form::hidden('status', false) !!}
                    <?php
                    $array_atr = ['id' => 'someSwitchOptionSuccess'];
                    if (!empty($model) && $model != '') {
                        if ($model->status == 1) {
                            $array_atr['checked'] = 'checked';
                        }
                    } else {
                        $array_atr['checked'] = 'checked';
                    }
                    ?>
                    {!! Form::checkbox('status', 1, null, $array_atr) !!}
                    <label for="someSwitchOptionSuccess" class="label-success"></label>
                </div>
            </div>
        </div>
    </div>
    <div class="box box-danger">
        <div class="box-header">
            <h3 class="box-title">Chương trình tour</h3>
            <!-- tools box -->
            <div class="pull-right box-tools">
                <button type="button" class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
            </div>
            <!-- /. tools -->
        </div>
        <div class="box-body pad">
            <!-- Content Field -->
            <div class="form-group col-sm-12 col-lg-12">
                {!! Form::label('content', 'Chi tiết tour :') !!}
                {!! Form::textarea('content', (!empty($model)) ? $model->content : old('content'), ['placeholder'=>'Chương trình tour', 'class' => 'form-control', 'id' => 'editor1' ]) !!}
            </div>
        </div>
    </div>
    <div class="box box-info">
        <div class="box-header">
            <h3 class="box-title">Điểm nhấn hành trình</h3>
            <!-- tools box -->
            <div class="pull-right box-tools">
                <button type="button" class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
            </div>
            <!-- /. tools -->
        </div>
        <div class="box-body pad">
            <!-- Content Field -->
            <div class="form-group col-sm-12 col-lg-12">
                {!! Form::label('content_info', 'Chi tiết tour :') !!}
                {!! Form::textarea('content_info', (!empty($model)) ? $model->content_info : old('content_info'), ['placeholder'=>'Chi tiết Tour', 'class' => 'form-control', 'id' => 'editor2' ]) !!}
            </div>
        </div>
    </div>
    <!-- Submit Field -->
    <div class="form-group col-sm-12 text-center">
        {!! Form::submit('Lưu', ['class' => 'btn btn-primary']) !!}
        <a href="{!! route('products.index') !!}" class="btn btn-default">Cancel</a>
    </div>
</div>


@section('scripts')
    <script src="/plugins/inputmask/inputmask.min.js" type="text/javascript"></script>
    <script src="/plugins/inputmask/jquery.inputmask.min.js" type="text/javascript"></script>
    <script src="/plugins/inputmask/bindings/inputmask.binding.min.js"></script>
    <script src="/plugins/inputmask/inputmask.numeric.extensions.min.js"></script>
    <script>
        $("#file-1").fileinput({
            // theme : 'explorer',
            title: "Please upload the attachment.",
            // uploadUrl: "/admin/product/upload-image",
            fileActionSettings : {
                // showUpload : false,
                showRemove : true

            },
            // uploadAsync: false,
            // showUpload: false, // hide upload button
            // showRemove: false, // hide remove button
            overwriteInitial: false, // append files to initial preview
            // minFileCount: 1,
            maxFileCount: 4,
            initialPreview: [
                <?php if (!empty($product_images)) { foreach ($product_images as $product_image) {?>
                    "<?php echo $product_image ?>",
                <?php }} ?>
            ],
            initialPreviewConfig: [
                    <?php if (!empty($product_images)) { foreach ($product_images as $id => $product_image) {?>
                        {caption: "", size: 576237, width: "120px", url: "/admin/product/delete-image/<?php echo $id ?>", key: '<?php echo $id ?>'},
                    <?php }} ?>
            ],

            initialPreviewAsData: true, // defaults markup
            // initialPreviewAsDataiewFileType: 'image', // image is the default and can be overridden in config below
            // uploadExtraData: function(previewId, index) {
            //     return {key: index};
            // },
            initialPreviewShowDelete:true,
            fileinput: {
                maxFileSize: 10240,
                // maxFileCount:4
            },
            callback:function(fileIds,oldfileIds){
                //Update fileIds
                this.showFiles({
                    fileIds:fileIds
                });
            }
        })
        //     .on("filebatchselected", function(event, files) {
        //     $("#file-1").fileinput("upload");
        // })
        ;

        $('#date_end').datepicker({
            viewMode: 'years'
        });

        $('#category_id').select2({
            placeholder: '---Tất cả---'
        });

        $('#transport').select2({
            placeholder: '---Tất cả---'
        });

        $('#start_point').select2({
            placeholder: '---Tất cả---'
        });

        $('#end_point').select2({
            placeholder: '---Tất cả---'
        });

        $('#size').select2({
            placeholder: '---Tất cả---'
        });

        $('#formid').on('keyup keypress', function(e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) {
                e.preventDefault();
                return false;
            }
        });

        $(function () {
            CKEDITOR.replace('editor1',{
                height: '200px',
                language:'vi',
                filebrowserBrowseUrl :'/plugins/ckfinder/ckfinder.html',
                filebrowserImageBrowseUrl : '/plugins/ckfinder/ckfinder.html?type=Images',
                filebrowserFlashBrowseUrl : '/plugins/ckfinder/ckfinder.html?type=Flash',
                filebrowserUploadUrl : '/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
                filebrowserImageUploadUrl : '/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
                filebrowserFlashUploadUrl : '/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash',
                customConfig: '/plugins/ckeditor/config.js'
            });
        });

        $(function () {
            CKEDITOR.replace('editor2',{
                height: '200px',
                language:'vi',
                filebrowserBrowseUrl :'/plugins/ckfinder/ckfinder.html',
                filebrowserImageBrowseUrl : '/plugins/ckfinder/ckfinder.html?type=Images',
                filebrowserFlashBrowseUrl : '/plugins/ckfinder/ckfinder.html?type=Flash',
                filebrowserUploadUrl : '/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
                filebrowserImageUploadUrl : '/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
                filebrowserFlashUploadUrl : '/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash',
                customConfig: '/plugins/ckeditor/config.js'
            });
        })
    </script>
@endsection
