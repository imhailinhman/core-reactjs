<div class="div_option">
    <div class="col-xs-12 col-md-2 col-sm-2">
        <div class="form-group">
            <label class="control-label">Tìm kiếm</label>
            <input id="search_key" class="form-control search_name enter-submit" name="search_key" placeholder="Tìm kiếm theo email, sdt, tên ..." value="{{ request('search_key') }}"/>
        </div>

    </div>

    <div class="col-xs-12 col-md-2 col-sm-2">
        <div class="form-group">
            <label class="control-label">Thời gian</label>
            <select name="date_time" class="form-control date_time" id="date_time">
                <option value="1" {!! (request('date_time') == 1) ? 'selected="selected"' : '' !!}>Hôm nay</option>
                <option value="2" {!! (request('date_time') == 2) ? 'selected="selected"' : '' !!}>Hôm qua</option>
                <option value="3" {!! (request('date_time') == 3) ? 'selected="selected"' : '' !!}>Tuần này</option>
                <option value="4" {!! (request('date_time') == 4) ? 'selected="selected"' : '' !!}>Tuần trước</option>
                <option value="5" {!! (request('date_time') == 5) || empty(request('from_date')) ? 'selected="selected"' : '' !!}>Tháng này</option>
                <option value="6" {!! (request('date_time') == 6) ? 'selected="selected"' : '' !!}>Tháng trước</option>
                <option value="7" {!! (request('date_time') == 7) ? 'selected="selected"' : '' !!}>Năm này</option>
                <option value="8" {!! (request('date_time') == 8) ? 'selected="selected"' : '' !!}>Năm trước</option>
            </select>
        </div>
    </div>

    <div class="col-xs-12 col-md-2 col-sm-2">
        <div class="form-group">
            <label class="control-label">Từ ngày </label>
            <input type="text" class="form-control datepicker" name="from_date" id="from_date"
                   placeholder="Từ ngày...." value="{{ !empty(request('from_date')) ? request('from_date') : date('d/m/Y', strtotime('first day of this month')) }}"/>
        </div>
    </div>
    <div class="col-xs-12 col-md-2 col-sm-2">
        <div class="form-group">
            <label class="control-label">Đến ngày </label>
            <input type="text" class="form-control datepicker" name="to_date" id="to_date"
                   placeholder="Đến ngày...." value="{{ !empty(request('to_date')) ? request('to_date') : date('d/m/Y', strtotime('last day of this month')) }}"/>
        </div>
    </div>

    <div class="col-xs-12 col-md-2 col-sm-6">
        <div class="form-group">
            <label class="control-label">Giới tính</label>
            <select name="filter_gender" class="form-control filter_header" id="gender">
                <option value=""> --- Lọc theo giới tính --- </option>
                <option value="0" {!! (request('filter_gender') == '0') ? 'selected="selected"' : '' !!}>Nam</option>
                <option value="1" {!! (request('filter_gender') == '1') ? 'selected="selected"' : '' !!}>Nữ</option>
                <option value="2" {!! (request('filter_gender') == '2') ? 'selected="selected"' : '' !!}>Giới tính</option>
            </select>
        </div>
    </div>

    <div class="col-xs-12 col-md-2 col-sm-6">
        <div class="form-group">
            <label class="control-label">Trạng thái</label>
            <select name="filter_status" class="form-control filter_header" id="status">
                <option value=""> --- Lọc theo trạng thái --- </option>
                <option value="1" {!! (request('filter_status') == '1') ? 'selected="selected"' : '' !!}>Hoạt động</option>
                <option value="0" {!! (request('filter_status') == '0') ? 'selected="selected"' : '' !!}>Ngừng hoạt động</option>
            </select>
        </div>
    </div>

    <div class="col-xs-12 col-md-2 col-sm-2">
        <div class="div_option wrap-filters ">
            <button class="btn btn-success form-group" type="submit">TÌm kiếm </button>
            <a class="btn btn-default" href="{{ route("users.index") }}">Clear</a>

            <div class="clearfix"></div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
