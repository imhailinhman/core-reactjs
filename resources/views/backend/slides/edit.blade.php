@extends('backend.layouts.app')

@section('breadcrumb')
    <li><a href="{!! route('slides.index') !!}">Danh sách slide</a></li><li class="active">Cập nhật slide {!! $model->title !!}</li>
@endsection

@section('content')
    <section class="content-header">
        <h1>
            Sửa : {!! $model->title !!}
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => ['slides.update', $model->id], 'class' => 'form jsvalidation', 'novalidate' => 'novalidate', 'files' => true , 'enctype' => 'multipart/form-data']) !!}

                    @include('backend.slides.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
