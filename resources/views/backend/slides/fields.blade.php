<div class="form-group col-md-12 col-sm-12 col-xs-12 text-center">
    <img id="img-upload" class="img-fluid center-block" width="400px" height="400px"/>
    <div id="img-upload-remove">
        @if (!empty($model->image))
            <img src="{{$model->image }}" class=" img-fluid img-thumbnail center-block "
                 width="400px" height="400px">
            <input type="hidden" name="image_tmp" value="{{$model->image}}"/>
        @else
            <img src="{{ config('filepath.no_image') }}" class="  img-fluid img-thumbnail center-block "
                 width="400px" height="400px">

        @endif
    </div>

    <div class="form-group col-md-offset-5 col-sm-offset-5 col-md-2 col-sm-2 col-xs-12 text-center">
        {!! Html::decode(Form::label('image','Ảnh',['class' => 'font-weight-bold float-left mr-4'])) !!}
        {!! Form::file('image', ['class' => 'form-control w-75', 'accept' => 'image/*', 'name' => 'image', 'placeholder' => 'Ảnh', 'id' => 'image' ]) !!}
    </div>
</div>


<!-- company_id Field -->
<div class="form-group col-sm-12">
    {!! Form::label('new_id', 'Tin tức:') !!}
    <span class="text-danger">(*)</span>
    <select name="new_id" id="new_id" class="form-control select2">
        <option value="">-------- Tin tức --------</option>
        @if(!empty($news))
            @foreach($news as $k_id => $new)
                <?php
                if (!empty($model)) {
                    $selected = ($k_id == $model->new_id) ? 'selected' : null;
                } else {
                    $selected = '';
                }
                ?>
                <option {{$selected}} value="{{ $k_id }}" {{ (old("new_id") == $k_id ? "selected":"") }}>{{ $new }}</option>
            @endforeach
        @endif
    </select>
</div>


{{--<!-- Title Field -->--}}
{{--<div class="form-group col-sm-6">--}}
    {{--{!! Form::label('title', 'Tên slide:') !!}--}}
    {{--<span class="text-danger">(*)</span>--}}
    {{--{!! Form::text('title', (!empty($model)) ? $model->title : old('title'), ['placeholder'=>'Tên slide', 'class' => 'form-control']) !!}--}}
{{--</div>--}}


<!-- Status Field -->
<div class="form-group col-sm-12 col-xs-12 col-md-12">
    {!! Form::label('status', 'Trạng thái :') !!}
    <div class="material-switch float-left">
        {!! Form::hidden('status', false) !!}
        <?php
        $array_atr = ['id' => 'someSwitchOptionSuccess'];
        if (!empty($model) && $model != '') {
            if ($model->status == 1) {
                $array_atr['checked'] = 'checked';
            }
        } else {
            $array_atr['checked'] = 'checked';
        }
        ?>
        {!! Form::checkbox('status', 1, null, $array_atr) !!}
        <label for="someSwitchOptionSuccess" class="label-success"></label>
    </div>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12 text-center">
    {!! Form::submit('Lưu', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('slides.index') !!}" class="btn btn-default">Cancel</a>
</div>
