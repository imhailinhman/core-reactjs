<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $category->id !!}</p>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'Title:') !!}
    <p>{!! $category->title !!}</p>
</div>

<!-- Title Seo Field -->
<div class="form-group">
    {!! Form::label('title_seo', 'Title Seo:') !!}
    <p>{!! $category->title_seo !!}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{!! $category->description !!}</p>
</div>

<!-- Description Seo Field -->
<div class="form-group">
    {!! Form::label('description_seo', 'Description Seo:') !!}
    <p>{!! $category->description_seo !!}</p>
</div>

<!-- Keyword Seo Field -->
<div class="form-group">
    {!! Form::label('keyword_seo', 'Keyword Seo:') !!}
    <p>{!! $category->keyword_seo !!}</p>
</div>

<!-- Avatar Field -->
<div class="form-group">
    {!! Form::label('avatar', 'Avatar:') !!}
    <p>{!! $category->avatar !!}</p>
</div>

<!-- Thumb Field -->
<div class="form-group">
    {!! Form::label('thumb', 'Thumb:') !!}
    <p>{!! $category->thumb !!}</p>
</div>

<!-- Alias Field -->
<div class="form-group">
    {!! Form::label('alias', 'Alias:') !!}
    <p>{!! $category->alias !!}</p>
</div>

<!-- Slug Field -->
<div class="form-group">
    {!! Form::label('slug', 'Slug:') !!}
    <p>{!! $category->slug !!}</p>
</div>

<!-- Parent Id Field -->
<div class="form-group">
    {!! Form::label('parent_id', 'Parent Id:') !!}
    <p>{!! $category->parent_id !!}</p>
</div>

<!-- Home Field -->
<div class="form-group">
    {!! Form::label('home', 'Home:') !!}
    <p>{!! $category->home !!}</p>
</div>

<!-- Hot Field -->
<div class="form-group">
    {!! Form::label('hot', 'Hot:') !!}
    <p>{!! $category->hot !!}</p>
</div>

<!-- Focus Field -->
<div class="form-group">
    {!! Form::label('focus', 'Focus:') !!}
    <p>{!! $category->focus !!}</p>
</div>

<!-- Sort Field -->
<div class="form-group">
    {!! Form::label('sort', 'Sort:') !!}
    <p>{!! $category->sort !!}</p>
</div>

<!-- Lang Field -->
<div class="form-group">
    {!! Form::label('lang', 'Lang:') !!}
    <p>{!! $category->lang !!}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{!! $category->status !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $category->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $category->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $category->deleted_at !!}</p>
</div>

