

<!-- Profile Image -->
<div class="col-md-offset-3 col-sm-offset-3 col-md-6 col-sm-6 col-xs-12">
    <div class="box-body box-profile">
        <h3 class="profile-username text-center">  {!! $admin->fullname !!}</h3>
        <div id="crop-avatar" class="text-center"
             style="background: url('{!! (!empty($admin->avatar)) ? $admin->avatar : config('filepath.no_image') !!}') no-repeat center center; background-size: cover;">
        </div>
        <ul class="list-group list-group-unbordered">
            <li class="list-group-item">
                <b>Tài khoản : </b> <a class="pull-right">{!! $admin->username !!}</a>
            </li>
            <li class="list-group-item">
                <b>Tên đầy đủ : </b> <a class="pull-right">{!! $admin->fullname !!}</a>
            </li>
            <li class="list-group-item">
                <b>Email : </b> <a class="pull-right">{!! $admin->email !!}</a>
            </li>

            <li class="list-group-item">
                <b>Trạng thái : </b>
                @if ($admin->status == 1)
                    <a class='pull-right btn  btn-xs btn btn-success btn-edit item_actions btn-mg-2'>Kích hoạt</a>
                @else
                    <a class='pull-right btn btn-xs btn-danger  btn-edit item_actions btn-mg-2'>Vô hiệu</a>
                @endif
            </li>

        </ul>
    </div>

</div>
<!-- /.box-body -->
