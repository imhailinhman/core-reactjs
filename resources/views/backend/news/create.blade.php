@extends('backend.layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Tạo tin tức
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'news.store', 'enctype' => 'multipart/form-data', 'id' => 'formid' ]) !!}

                        @include('backend.news.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
