@extends('backend.layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Cập nhật tin tức
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::open(['route' => ['news.update', $model->id], 'class' => 'form jsvalidation', 'novalidate' => 'novalidate', 'files' => true , 'enctype' => 'multipart/form-data', 'id' => 'formid']) !!}

                   @include('backend.news.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection
