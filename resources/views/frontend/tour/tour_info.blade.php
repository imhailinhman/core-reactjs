@extends('frontend.layouts.master')
@section('content')
<div class="container">



    <!-- breadcrumb -->
    <div class="n3-breadcrumb mt-30">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 ">
                    <div class="breadcrumb">
                        <span><a href="" itemprop="url"><span itemprop="title">Du lịch</span></a></span> »
                        <span><a href="" itemprop="url"><span itemprop="title">Du lịch trong nước</span></a></span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container n3-tour-description mg-bot40 mt-15">

        <div class="row">
            <div class="col-xs-12 mg-bot15">
                @if (!empty($product))
                    <h1 class="tour-name" itemprop="name"><a>{{ $product->name }}</a></h1>
                @endif
            </div>
            <div class="slideshow-pt col-lg-8 col-md-12 col-sm-12 col-xs-12 pos-relative">
                <div id="myCarouse2" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        @php
                            $slide_active = $product_imgs->last()['id'];
                        @endphp
                        @if(!$product_imgs->isEmpty())
                            @foreach($product_imgs as $key => $product_img)
                                <li data-target="#myCarouse2" data-slide-to="{{ $key }}" class="@if(!empty($slide_active) && $product_img->id == $slide_active) active @endif"></li>
                            @endforeach
                        @endif
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        @if(!$product_imgs->isEmpty())
                            @foreach($product_imgs as $product_img)
                                <div class="item @if(!empty($slide_active) && $product_img->id == $slide_active) active @endif">
                                    <img src="{{ $product_img->image }}" alt="{{ $product_img->title }}" class="img-responsive pic-ss-pt">
                                </div>

                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
            <div class="info col-lg-4 col-md-12 col-sm-12 col-xs-12">
                <div class="frame-info pos-relative">
                    <div class="sec2">
                        <div class="row">
                            <div class="col-md-12 col-xs-12">
                                <div class="row border-bt mg-bot10">
                                    <div class="col-lg-6 col-md-3 col-sm-3 col-xs-6">Ngày Khởi hành:</div>
                                    <div class="col-lg-6 col-md-10 col-sm-9 col-xs-6">
                                        <div class="mg-bot-date">
                                            {{ date('d/m/Y', strtotime($product->date_start)) }}
                                        </div>
                                    </div>
                                </div>
                                <div class="row border-bt mg-bot10">
                                    <div class="col-lg-6 col-md-2 col-sm-3 col-xs-6" style="padding-right: 0px !important;">Nơi khởi hành:</div>
                                    <div class="col-lg-6 col-md-10 col-sm-9 col-xs-6">
                                        {{ $product->startPoint->province_name }}
                                    </div>
                                </div>
                                <div class="row border-bt mg-bot10">
                                    <div class="col-lg-6 col-md-3 col-sm-3 col-xs-6">Ngày về:</div>
                                    <div class="col-lg-6 col-md-10 col-sm-9 col-xs-6">
                                        <div class="mg-bot-date">
                                            {{ date('d/m/Y', strtotime($product->date_end)) }}
                                        </div>
                                    </div>
                                </div>
                                <div class="row border-bt mg-bot10">
                                    <div class="col-lg-6 col-md-2 col-sm-3 col-xs-6" style="padding-right: 0px !important;">Nơi xuất phát về:</div>
                                    <div class="col-lg-6 col-md-10 col-sm-9 col-xs-6">
                                        {{ $product->endPoint->province_name }}
                                    </div>
                                </div>
                                <div class="row border-bt mg-bot10">
                                    <div class="col-lg-6 col-md-2 col-sm-3 col-xs-6">Tổng thời gian:</div>
                                    <div class="col-lg-6 col-md-10 col-sm-9 col-xs-6">
                                        {{ $product->sum_time }}
                                    </div>
                                </div>
                                <div class="price-tour wow animated pulse mt-15" data-wow-delay="300ms" data-wow-iteration="infinite" data-wow-duration="2s" >
                                    @if ($product->price > 0 && $product->price_sale > 0)
                                        Giá từ : <span class="price">{{ number_format($product->price, 0, '', ',') }} đ</span>&nbsp
                                        <span class="price-o">{{ number_format($product->price_sale, 0, '', ',') }} đ</span>
                                    @elseif($product->price > 0 && $product->price_sale == 0)
                                        Giá từ : <span class="price">{{ number_format($product->price, 0, '', ',') }} đ</span>&nbsp
                                    @elseif($product->price == 0)
                                        Giá từ : Đang cập nhật
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="btn-dattour text-center">
                        <button>
                            Đặt ngay
                        </button>
                    </div>
                    <div class="title-price-tour mt-30 text-center">
                        <h3>HOTLINE TƯ VẤN & ĐẶT TOUR</h3>
                        <span class="mt-15 text-center">
                        <a href="tel:0933467875">
                            <span class="glyphicon glyphicon-phone mt-15" aria-hidden="true" style="font-size: 18px; color: red"></span>
                            <span style="font-size: 18px; color: red; font-weight: bold">0933467875</span>
                        </a>
                    </span>
                    </div>

                </div>
            </div>

        </div>
    </div>

    <div class="container n3-tour-detail">
        <div class="row">
            <div class="col-lg-4 col-md-3 col-sm-4 col-xs-12">
                <div class="menu-left mg-bot30">
                    <div class="panel panel-default panel-side-menu">
                        <div class="panel-body panel-body-nav">
                            <div class="side-menu">
                                <ul class="list-unstyled">
                                    <li id="cct" class="tab-chuongtrinhtour active">
                                        <a data-toggle="tab" href="#chuongtrinhtour_id">
                                            <i class="fas fa-spinner"></i>
                                            <span>Chương trình tour</span>
                                        </a>
                                    </li>
                                    <li class="tab-chitiettour" id="tabChiTiet">
                                        <a data-toggle="tab" href="#chitiettour_id">
                                            <i class="fas fa-list"></i>
                                            <span>Điểm nhấn hành trình</span>
                                        </a>
                                    </li>
                                    <li class="tab-ykien">
                                        <a data-toggle="tab" href="#tab-ykien">
                                            <i class="far fa-comments"></i>
                                            <span>Ý kiến khách hàng</span>
                                        </a>
                                    </li>

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-8 col-md-9 col-sm-8 col-xs-12">
                <div class="sec-info tab-content">
                    <div class="chuongtrinhtour mg-bot30 tab-pane fade in active" id="chuongtrinhtour_id">
                        <div class="title-lg"><h2>Chương trình tour</h2></div>
                        <div class="sec-content itinerary">
                            {!!  !empty($product) ? $product->content : ""!!}
                        </div>
                    </div>

                    <div class="chitiettour mg-bot30 tab-pane fade" id="chitiettour_id">
                        <div class="title-lg"><h2>Điểm nhấn hành trình</h2></div>
                        <div class="sec-content tour-detail">
                            {!!  !empty($product) ? $product->content_info : ""!!}
                        </div>
                    </div>

                    <div class="ykien mg-bot30 tab-pane fade" id="tab-ykien">
                        <!--Ý kiến khách hàng-->

                        <div class="title-lg">&#221; KIẾN KH&#193;CH H&#192;NG (0)</div>
                        <div class="sec-content customer-idea">
                            <form action="/Comment/Reply" id="mainform" method="post">
                                <input name="__RequestVerificationToken" type="hidden" value="niWw5_n5UOWGzUVhsEsED4R-jXihzTAJZQ-TKetC1X1vz3_gn7xNfFjYdQk0XDbhqs9uuOL4lGjkZYfn4beoPB6etpmXCeJCXW3d2VWRiSs1">
                                <input id="HeaderTour" name="HeaderTour" type="hidden" value="NDSGN847">
                                <div class="frame-feedback">
                                    <div class="title">
                                        <img src="{{asset('')}}layouts/images/i-feedback.png" alt="feedback">&nbsp;&nbsp;Gửi &#253; kiến
                                    </div>
                                    <div class="form-feedback">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-12 col-xs-12 mg-bot15">
                                                <input class="form-control input-md" id="UserName" name="UserName" placeholder="Họ tên(*)" required="required" type="text" value="">
                                            </div>
                                            <div class="col-md-6 col-sm-12 col-xs-12 mg-bot15">
                                                <input class="form-control input-md" id="Email" name="Email" placeholder="Email(*)" required="required" type="email" value="">
                                            </div>
                                        </div>
                                        <div class="row mg-bot15">
                                            <div class="col-xs-12">
                        <textarea class="form-control" cols="20" id="Content" name="Content" placeholder="Ý kiến của bạn(*)" required="required" rows="5">
</textarea>
                                            </div>

                                        </div>

                                        <div class="row">
                                            <div class="col-xs-12 text-center mg-bot15">
                                                <button type="submit" class="btn btn-md btn-detail" id="btn_submitmainform">Gửi đi&nbsp;&nbsp;<i class="fas fa-paper-plane"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </form>
                            <div class="clear"></div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="col-xs-12">
                <div class="line-ct"></div>
            </div>
            <div class="col-xs-12 tour-similar">
                <div class="row">
                    <div class="col-xs-12 mg-bot20">
                        <div class="title-ft">Các tour liên quan</div>
                    </div>
                    <div id="theogia">
                        @if(!$product_interdepend->isEmpty())
                            @foreach($product_interdepend as $product)
                                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 mg-bot30">
                                    <a href="/{{ $category->slug."/".$product->title_seo }}" title="{{ $product->name }}">
                                        <div class="pos-relative">
                                            <img src="{{ $product->image }}" class="img-responsive pic-ttt" alt="{{ $product->name }}">
                                            <div class="frame-ttt1">
                                                <div class="f-left">
                                                    <img src="{{asset('')}}layouts/images/i-date-w.png" alt="date">
                                                </div>
                                                <div class="f-left date">
                                                    <span class="yellow">{{ date('d/m/Y', strtotime($product->date_start)) }}</span>
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                        </div>
                                    </a>
                                    <div class="frame-ttt2">
                                        <a href="/{{ $category->slug."/".$product->title_seo }}" title="{{ $product->name }}">
                                            <div class="ttt-title dot-dot cut-ttt">{!! str_limit($product->name, $limit = 60, $end = '...') !!}</div>
                                        </a>
                                        <div class="ttt-line"></div>
                                        <div>
                                            <img src="{{asset('')}}layouts/images/i-price.png" class="f-left" alt="price">
                                            <div class="f-left ttt-info">

                                                <span class="price-n">{{ number_format($product->price, 0, '', ',') }} đ</span>
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>

                </div>
            </div>

        </div>
    </div>



</div>
@endsection
