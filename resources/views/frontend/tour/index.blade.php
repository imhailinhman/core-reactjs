@extends('frontend.layouts.master')
@section('content')
<div class="container">

    <!-- breadcrumb -->
    <div class="n3-breadcrumb mt-30">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 ">
                    <div class="breadcrumb">
                        <span><a href="" itemprop="url"><span itemprop="title">Du lịch</span></a></span> »
                        <span><a href="" itemprop="url"><span itemprop="title">Du lịch trong nước</span></a></span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- new-bar -->
    <div class="container n3-list-tour mg-bot40">
        <div class="row">
            {{--<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 mg-bot30">--}}
                {{--<div class="new-bar">--}}
                    {{--<a href="tour.html"><div class="title mb-0 border-bottom-0">Tour trong nước ngoài</div></a>--}}
                    {{--<a href="tour_info.html" class="mg-bot20 w-100">--}}
                        {{--<div class="boder-dashed clearfix"></div>--}}
                        {{--<div class="new-hot">--}}
                            {{--<div class="img-new col-md-12 col-sm-12 col-xs-12">--}}
                                {{--<img src="{{asset('')}}layouts/images/tet-doc-lap-moc-chau.jpg" alt="" class="w-100">--}}
                            {{--</div>--}}
                            {{--<div class=" text-center col-md-12 col-sm-12 col-xs-12 mg-bot15 mt-10">--}}
                                {{--Nha Trang - Đà Lạt--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</a>--}}
                    {{--<a href="tour_info.html" class="mg-bot20 w-100">--}}
                        {{--<div class="boder-dashed clearfix"></div>--}}
                        {{--<div class="new-hot">--}}
                            {{--<div class="img-new col-md-12 col-sm-12 col-xs-12">--}}
                                {{--<img src="{{asset('')}}layouts/images/tet-doc-lap-moc-chau.jpg" alt="" class="w-100">--}}
                            {{--</div>--}}
                            {{--<div class=" text-center col-md-12 col-sm-12 col-xs-12 mg-bot15 mt-10">--}}
                                {{--Nha Trang - Đà Lạt--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</a>--}}
                    {{--<a href="tour_info.html" class="mg-bot20 w-100">--}}
                        {{--<div class="boder-dashed clearfix"></div>--}}
                        {{--<div class="new-hot">--}}
                            {{--<div class="img-new col-md-12 col-sm-12 col-xs-12">--}}
                                {{--<img src="{{asset('')}}layouts/images/tet-doc-lap-moc-chau.jpg" alt="" class="w-100">--}}
                            {{--</div>--}}
                            {{--<div class=" text-center col-md-12 col-sm-12 col-xs-12 mg-bot15 mt-10">--}}
                                {{--Nha Trang - Đà Lạt--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</a>--}}
                    {{--<a href="tour_info.html" class="mg-bot20 w-100">--}}
                        {{--<div class="boder-dashed clearfix"></div>--}}
                        {{--<div class="new-hot">--}}
                            {{--<div class="img-new col-md-12 col-sm-12 col-xs-12">--}}
                                {{--<img src="{{asset('')}}layouts/images/tet-doc-lap-moc-chau.jpg" alt="" class="w-100">--}}
                            {{--</div>--}}
                            {{--<div class=" text-center col-md-12 col-sm-12 col-xs-12 mg-bot15 mt-10">--}}
                                {{--Nha Trang - Đà Lạt--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</a>--}}
                {{--</div>--}}

            {{--</div>--}}
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="list-tour">
                    @if (!empty($category))
                        <div class="des-tour">
                            <h1 itemprop="name"><a href="#">{{ $category->title }}</a></h1>
                            <div class="boder-dashed"></div>
                            <div class="clear"></div>
                            <div class="des-content">
                                <div class="mg-bot10">
                                    <div class="mt-10">
                                        {{ $category->description }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                    <div class="list">
                        <div class="row">
                            <!-- ToursHot -->
                            <div class="col-md-12 text-center list-tour-title">
                                <h2 class="title">
                                    <a href="#">Danh sách {{ $category->title }}</a>
                                </h2>
                            </div>

                            <!--list tour-->
                            @if (!empty($products))
                                @foreach($products as $key => $product)
                                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="item mg-bot30">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <div class="tour-name">
                                                        <a href="{{ $category->slug."/".$product->title_seo }}" title="{{ $product->name }}"><h3>{{ $product->name }}</h3></a>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                                    <div class="pos-relative">
                                                        <a href="{{ $category->slug."/".$product->title_seo }}" title="{{ $product->name }}"><img src="{{ $product->image }}" class="img-responsive pic-lt" alt="{{ $product->name }}"></a>
                                                    </div>
                                                </div>
                                                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                                    <div class="frame-info">
                                                        <div class="row">
                                                            <div class="col-md-6 col-sm-6 col-xs-12 mg-bot10">
                                                                <div class="f-left l"><img src="{{asset('')}}layouts/images/i-code.png" alt="code"></div>
                                                                <div class="f-left r">{{ $product->code_tour }}</div>
                                                                <div class="clear"></div>
                                                            </div>
                                                            <div class="col-md-6 col-sm-6 col-xs-12 mg-bot10">
                                                                <div class="f-left l"><img src="{{asset('')}}layouts/images/i-chair.png" alt="chair"></div>
                                                                <div class="f-left r">
                                                                    Số chỗ: <span class="font500">{{ $product->seat }}</span>
                                                                </div>
                                                                <div class="clear"></div>
                                                            </div>
                                                            <div class="col-md-6 col-sm-6 col-xs-12 mg-bot10">
                                                                <div class="f-left l"><img src="{{asset('')}}layouts/images/i-date.png" alt="date"></div>
                                                                <div class="f-left r">Ngày đi: <span class="font500">{{ date('d/m/Y', strtotime($product->date_start)) }}</span></div>
                                                                <div class="clear"></div>
                                                            </div>
                                                            <div class="col-md-6 col-sm-6 col-xs-12 mg-bot10">
                                                                <div class="f-left l"><img src="{{asset('')}}layouts/images/i-clock.png" alt="clock"></div>
                                                                <div class="f-left r">Ngày về: <span class="font500">{{ date('d/m/Y', strtotime($product->date_end)) }}</span></div>
                                                                <div class="clear"></div>
                                                            </div>
                                                            <div class="col-md-6 col-sm-6 col-xs-12 mg-bot10">
                                                                <div class="f-left l"><img src="{{asset('')}}layouts/images/i-price.png" alt="price"></div>
                                                                <div class="f-left r">
                                                                    Giá: <span class="font500 price">{{ number_format($product->price, 0, '', ',') }} đ</span>
                                                                </div>
                                                                <div class="clear"></div>
                                                            </div>
                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                <div class="f-left l"><img src="{{asset('')}}layouts/images/i-date.png" alt="date"></div>
                                                                <div class="f-left r">Số ngày: <span class="font500">{{ $product->sum_time }}</span></div>
                                                                <div class="clear"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                            <div id="divEndTour"></div>
                        </div>
                    </div>

                        <div class="col-xs-12 text-right mg-bot30">
                            <div class="pager_simple_orange text-center">
                                @if(isset($products) && !empty($products))
                                    {!! $products->appends(request()->query())->links() !!}
                                @endif
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>

    <!--NEW-->
    <div class="container n3-tour-hour">
        <div class="row">
            <div class="col-xs-12">
                <h2 class="title mg-bot10">
                    <a href="">Tin tức liên quan</a>
                </h2>
            </div>
            <div id="NewTour">
                @if (!empty($news))
                    @foreach($news as $key => $new)
                        @php
                            $route = route('newInfo.index', ['id' => $new->slug]);
                            if($new->type === \App\Repositories\Interfaces\NewRepository::TYPE_PROMOTION) {
                                $route = route('promotionInfo.index', ['id' => $new->slug]);
                            }
                        @endphp

                        <div class="col-md-3 col-md-3 col-sm-6 col-xs-12 mg-bot30">
                    <a href="{{ $route }}" title="{{ $new->title }}">
                        <div class="pos-relative">
                            <img src="{{ $new->image }}" class="img-responsive pic-tgc" alt="{{ $new->title }}">
                        </div>
                        <div class="frame-tgc2">
                            <div class="tgc-title dot-dot-ajax cut-tgc ddd-truncated" style="overflow-wrap: break-word;">{!! str_limit($new->title, $limit = 30, $end = '...') !!}</div>
                            <div class="tgc-line"></div>
                            <div class="mg-bot10 text-center">
                                <div class="clear"></div>
                                <div class="text-right">
                                    <a href="{{ $route }}" class="view_more text-red" title="{{ $new->title }}">Xem thêm &nbsp;<i class="fas fa-long-arrow-alt-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
