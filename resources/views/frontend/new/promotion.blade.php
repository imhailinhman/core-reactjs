@extends('frontend.layouts.master')
@section('content')
<div class="container">

    <!-- breadcrumb -->
    <div class="n3-breadcrumb mt-30">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 ">
                    <div class="breadcrumb">
                        <span><a href="" itemprop="url"><span itemprop="title">Du lịch</span></a></span> »
                        <span><a href="" itemprop="url"><span itemprop="title">Du lịch trong nước</span></a></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container n3-news mg-bot40">
        <div class="row">
            <div class="col-xs-12 mg-bot30">
                <div class="title"><h1>Khuyến mại</h1></div>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 l">
                @if (!empty($news))
                    @foreach($news->shuffle() as $key => $new)
                        <div class="row mg-bot30">
                            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 mg-bot">
                                <a href="{{ route('newInfo.index', ['id' => $new->slug]) }}" title="{{ $new->title }}">
                                    <img src="{{ $new->image }}" class="img-responsive pic-news-l" alt="{{ $new->title }}">
                                </a>
                            </div>

                            <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
                                <div class="frame-news">
                                    <div class="frame-top">
                                        <h2 class="news-title-l">
                                            <a href="{{ route('promotionInfo.index', ['id' => $new->slug]) }}" title="{{ $new->title }}" class="dot-dot cut-name" style="overflow-wrap: break-word;">
                                                {{ $new->title }}
                                            </a>
                                        </h2>
                                        <div class="frame-date">
                                            <div class="f-left"><img src="{{asset('')}}layouts/images/i-date.png" alt="date"></div>
                                            <div class="f-left date">{{ date('d/m/Y', strtotime($new->created_at)) }}</div>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                    <div class="frame-bot">
                                        <div class="des-content dot-dot cut-content ddd-truncated" style="overflow-wrap: break-word;">
                                            {{ $new->description }}
                                        </div>
                                        <div class="text-right">
                                            <a href="{{ route('promotionInfo.index', ['id' => $new->slug]) }}" class="view_more" title="{{ $new->title }}">Xem thêm &nbsp;<i class="fas fa-long-arrow-alt-right"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif

                <div class="col-xs-12 text-right mg-bot30">
                    <div class="pager_simple_orange text-center">
                        @if(isset($news) && !empty($news))
                            {!! $news->appends(request()->query())->links() !!}
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>
@endsection
