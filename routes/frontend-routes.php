<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

 Route::get('', function () {
     return redirect()->route('home.index');
 });

Route::get('/trang-chu', function () {
    return redirect()->route('home.index');
});

Route::get('/admins', function () {
    return redirect()->route('admins.index');
});


Route::get('/admin', function () {
    return redirect()->route('admins.index');
});

Route::get('reactjs/', ['as' => 'reactjs.index', 'uses' => 'Frontend\HomeController@reactjs']);


// gọi ra trang view demo-pusher.blade.php
Route::get('demo-pusher/','Frontend\HomeController@getPusher');
// Truyển message lên server Pusher
Route::get('fire-event/','Frontend\HomeController@fireEvent');


Route::get('', ['as' => 'home.index', 'uses' => 'Frontend\HomeController@index']);
Route::get('tin-tuc/', ['as' => 'new.index', 'uses' => 'Frontend\HomeController@new']);
Route::get('tin-tuc/{slug}/', ['as' => 'newInfo.index', 'uses' => 'Frontend\HomeController@newInfo']);


Route::get('chuong-trinh-khuyen-mai/', ['as' => 'promotion.index', 'uses' => 'Frontend\HomeController@promotion']);
Route::get('chuong-trinh-khuyen-mai/{slug}/', ['as' => 'promotionInfo.index', 'uses' => 'Frontend\HomeController@promotionInfo']);


Route::get('lien-he/', ['as' => 'lienhe.index', 'uses' => 'Frontend\HomeController@lienhe']);
Route::post('lien-he/create', ['as' => 'lienhe.create', 'uses' => 'Frontend\HomeController@postlienhe']);

Route::get('tu-van-visa/', ['as' => 'visa.index', 'uses' => 'Frontend\HomeController@visa']);
Route::get('tu-van-visa/{slug}/', ['as' => 'visaInfo.index', 'uses' => 'Frontend\HomeController@visaInfo']);

Route::get('faq/', ['as' => 'faq_f.index', 'uses' => 'Frontend\HomeController@faq']);


Route::get('tim-kiem-tour/', ['as' => 'search.index', 'uses' => 'Frontend\HomeController@search']);

//Route::get('tour-du-lich/{tour}', ['as' => 'tour.index', 'uses' => 'Frontend\HomeController@tour']);
Route::get('{tour}', ['as' => 'tourInfo.index', 'uses' => 'Frontend\HomeController@tour']);
Route::get('{tour}/{slug}', ['as' => 'tourInfo.index', 'uses' => 'Frontend\HomeController@tourInfo']);


