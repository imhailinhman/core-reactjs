<?php

namespace App\Repositories\Caches;

use App\Repositories\Eloquents\DbDistrictRepository;
use App\Repositories\Interfaces\DistrictRepository;

class CacheDistrictRepository extends CacheRepository implements DistrictRepository
{
    function __construct(DbDistrictRepository $dbDistrictRepository)
    {
        $this->dbRepository = $dbDistrictRepository;
    }

    public function getIdBySlug($slug, $province_id)
    {
        return $this->dbRepository->getIdBySlug($slug, $province_id);
    }

}