<?php

namespace App\Repositories\Caches;

use App\Repositories\Eloquents\DbSettingRepository;
use App\Repositories\Interfaces\SettingRepository;

class CacheSettingRepository extends CacheRepository implements SettingRepository
{
    function __construct(DbSettingRepository $dbRepository)
    {
        $this->dbRepository = $dbRepository;

    }

    public function updateSetting($setting_data)
    {
        return $this->dbRepository->updateSetting($setting_data);
    }

}
