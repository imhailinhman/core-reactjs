<?php

namespace App\Repositories\Caches;

use App\Repositories\Eloquents\DbContactRepository;
use App\Repositories\Interfaces\ContactRepository;

class CacheContactRepository extends CacheRepository implements ContactRepository
{
    function __construct(DbContactRepository $dbRepository)
    {
        $this->dbRepository = $dbRepository;

    }

}
