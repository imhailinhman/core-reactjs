<?php

namespace App\Repositories\Caches;

use App\Repositories\Eloquents\DbSlideRepository;
use App\Repositories\Interfaces\SlideRepository;

class CacheSlideRepository extends CacheRepository implements SlideRepository
{
    function __construct(DbSlideRepository $dbRepository)
    {
        $this->dbRepository = $dbRepository;
    }


}
