<?php

namespace App\Repositories\Caches;

use App\Repositories\Eloquents\DbVisaRepository;
use App\Repositories\Interfaces\VisaRepository;

class CacheVisaRepository extends CacheRepository implements VisaRepository
{
    function __construct(DbVisaRepository $dbRepository)
    {
        $this->dbRepository = $dbRepository;

    }

}
