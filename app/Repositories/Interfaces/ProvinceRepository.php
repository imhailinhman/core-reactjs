<?php

namespace App\Repositories\Interfaces;


interface ProvinceRepository extends BaseRepository
{
    /**
     * @param $slug
     * @return bool
     */
    public function getIdBySlug($slug);

    /**
     * @param $slug
     * @return bool
     */
    public function getBySlug($slug);
}