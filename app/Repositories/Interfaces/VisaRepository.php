<?php

namespace App\Repositories\Interfaces;


interface VisaRepository  extends BaseRepository
{
    const STATUS_ACTIVE = 1;
    const STATUS_DEACTIVE = 0;

    const TYPE_DEFAULT = 0;
    const TYPE_PROMOTION = 1;

}
