<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

/**
 * Class
 * @package App\Models
 * @version December 3, 2018, 10:53 pm +07
 *
 * @property \App\Models\Category category
 * @property \App\Models\User user
 * @property \Illuminate\Database\Eloquent\Collection billDetail
 * @property string title
 * @property string description
 */
class Faq extends Model
{
//    use SoftDeletes;

    public $table = 'faq';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


//    protected $dates = ['deleted_at'];

    protected $guarded = [];

    /**
     * Validation rules
     *
     * @var array
     */

    public static $rules = [
        'title' => 'required|max:255',
    ];


    public static $messages = [
        'title.required' => 'Tiêu đề là trường bắt buộc!',
        'title.max' => 'Tiêu đề tối đa 255!',

    ];

}
