<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use InfyOm\Generator\Utils\ResponseUtil;
use Illuminate\Support\Collection;
use Response, Auth;

/**
 * @SWG\Swagger(
 *   basePath="/api/v1",
 *   @SWG\Info(
 *     title="Laravel Generator APIs",
 *     version="1.0.0",
 *   )
 * )
 * This class should be parent class for other API controllers
 * Class AppBaseController
 */
class AppBaseController extends Controller
{
    protected $perPage;

    public function __construct()
    {
//        $this->middleware('auth');
        $this->perPage = 10;
    }

    public function sendResponse($result, $message)
    {
        return Response::json(ResponseUtil::makeResponse($message, $result));
    }

    public function sendError($error, $code = 404)
    {
        return Response::json(ResponseUtil::makeError($error), $code);
    }

    public function responseApp($success, $code, $message, $response = null)
    {
        return Response::json([
            'meta' => [
                'success' => $success,
                'message' => $message,
                'code' => $code
            ],
            'response' => ($response === null) ? null : $this->transforms($response)
        ]);
    }


    /**
     * Transform data before response.
     *
     * @param collection $node
     * @return array
     */
    protected function transforms($nodes)
    {
        if ($nodes instanceof Model) {
            return $this->transformer($nodes);
        }

        if ($nodes instanceof LengthAwarePaginator) {
            $data = $nodes->getCollection()->transform(function ($node, $key) {
                return $this->transformer($node);

            });

            $nodes->setCollection($data);

            return $nodes;
        }

        return (($nodes instanceof Collection) ? $nodes : collect($nodes))->transform(function ($node, $key) {
            return $this->transformer($node);
        });
    }

    /**
     * Transform data before response.
     *
     * @param collection $node
     * @return array
     */
    protected function transformer($node)
    {
        return $node;
    }

}
