<?php

if (!function_exists('setting')) {
    function setting($key, $default = '')
    {
        static $settings;
        if (is_null($settings)) {
            $settings = Cache::get('settings');
        }
        return \Illuminate\Support\Arr::get($settings, $key, $default);
    }
}
