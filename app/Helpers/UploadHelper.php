<?php

namespace App\Helpers;


use Illuminate\Http\File;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class UploadHelper
{
    /**
     * @param UploadedFile $file
     * @param $agentId
     * @return mixed
     */
    public static function uploadImgFile(UploadedFile $file, $forderFile)
    {
        try {
            $fileName = round(microtime(true) * 1000);
//            $fileName = $file->getClientOriginalName();
            $path = $file->storeAs($forderFile,
                $fileName. '.' . $file->extension(),
//                $fileName,
                [
                    'disk' => 'public'
                ]);
            return Storage::disk('public')->url($path);
        } catch (\Exception $ex) {
            Log::error($ex->getMessage() . '. ' . $ex->getFile() . ':' . $ex->getLine());
            return false;
        }
    }

    public static function uploadImgFileProduct(UploadedFile $file, $forderFile)
    {
        try {
            $fileName = $file->getClientOriginalName();
            $path = $file->storeAs($forderFile,
                $fileName,
                [
                    'disk' => 'public'
                ]);

            return Storage::disk('public')->url($path);
        } catch (\Exception $ex) {
            Log::error($ex->getMessage() . '. ' . $ex->getFile() . ':' . $ex->getLine());
            return false;
        }
    }

}
