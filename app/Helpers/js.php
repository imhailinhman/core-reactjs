<?php

if (!function_exists('js')) {

    function js($name)
    {
        //get html
        if (!is_array($name)) {
            $name = [$name];
        }

        $js = [];
        foreach ($name as $n) {
            $asset = str_replace(['\\/', '/\\'], '/', "js/{$n}");
            if (strpos($asset, '.js') === false)
                $asset .= ".js";
            $url = asset($asset, is_secure());
            $js[] = "<script src=\"{$url}\" type=\"text/javascript\"></script>";
        }

        return implode("\n", $js);
    }
}